#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
import git
import logging
import re
import requests
from pathlib import Path

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def arg_parser():
    parser = argparse.ArgumentParser(description="download attachments in a testrun")

    parser.add_argument(
        "--lore-url",
        required=False,
        default="http://lore.kernel.org/stable",
        help="Url to lore mailing list",
    )

    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Display debug messages",
    )

    return parser


def scan_for_latest_repo(url):
    full_url = f"{url}/_/text/mirror/"
    logger.debug(f"{full_url=}")
    resp = requests.get(full_url)
    last_git = None
    if resp.status_code >= 200 and resp.status_code < 300:
        for line in resp.text.split('\n'):
            if re.match(f".*?{url}/\\d+.*", str(line)):
                # isolate the url and set it to be last one seen
                fields = line.split('"')
                logger.debug(f"{fields=}")
                for x in fields:
                    if x.startswith('http'):
                        last_git = x

    return last_git


if __name__ == "__main__":
    args = arg_parser().parse_args()
    if args.debug:
        logger.setLevel(level=logging.DEBUG)
    logger.debug(f"{args.lore_url=}")
    url = scan_for_latest_repo(args.lore_url)
    to_path = f"./mailinglists/{args.lore_url.split('/')[-1]}"

    logger.debug(f"argh {to_path}/{Path(url).stem}")
    repo = Path(f"{to_path}/{Path(url).stem}")
    if not repo.exists():
        # if none must be empty
        print(f"Clone repository from {format(url)}")
        repo = git.Repo.clone_from(url, (to_path+"/"+Path(url).stem))

    for repo_path in Path(to_path).iterdir():
        repo = git.Repo(repo_path.as_posix())

        print(f"git pull in repo path {repo_path}")
        repo.git.reset("--hard")
        repo.git.clean("-xdf")
        repo.git.checkout("master")
        repo.git.pull()
